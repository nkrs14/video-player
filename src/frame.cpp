#include "frame.hpp"

sakurajin::Frame::Frame(){};

bool sakurajin::Frame::isEmpty() const {
    return frameData==nullptr;
}

sakurajin::Frame::~Frame(){
    clear();
}

sakurajin::Frame::Frame(AVFrame* dat){
    frameData = dat;
}

AVFrame* sakurajin::Frame::data() const{
    return frameData;
}

void sakurajin::Frame::clear(){
    av_frame_free(&frameData);
    av_free(frameData);
}

sakurajin::Frame::operator AVFrame*(){
    return frameData;
}

sakurajin::Frame& sakurajin::Frame::operator=(AVFrame* x){
    if (x == frameData){return *this;};
    if(frameData != nullptr){
        av_frame_free(&frameData);
        av_free(frameData);
    }
    frameData = x;
    return *this;
}

