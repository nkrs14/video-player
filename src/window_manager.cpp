#include "window_manager.hpp"

#include <iostream>
#include <filesystem>
#include <sstream>
#include <fstream>

void sakurajin::framebuffer_size_callback(GLFWwindow* window, int width, int height){
    glViewport(0, 0, width, height);
}

void sakurajin::processInput(GLFWwindow *window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

GLFWwindow* sakurajin::create_window(bool& error){
    //init glwf and set basic opengl parameters
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    //create the empty window
    GLFWwindow* window = glfwCreateWindow(800, 600, "Basic video player", NULL, NULL);
    if (window == NULL){
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        error = true;
        return nullptr;
    }
    glfwMakeContextCurrent(window);

    //check if glad is loaded properly
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        std::cerr << "Failed to initialize GLAD" << std::endl;
        error = true;
        return nullptr;
    }
    
    //change the size of the framebufer when the window is resized
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    
    error = false;
    return window;
}

void sakurajin::cleanup(){
    //properly end glfw
    glfwTerminate();
}

int sakurajin::createShader ( unsigned int& shaderProgram ) {
    
    //check if shader files exist
    std::filesystem::path vertLoc = "data/shader.vert";
    if(vertLoc.empty()){
        std::cerr << "cannot find vertex shader" << std::endl;
        return -1;
    }
    
    std::filesystem::path fragLoc = "data/shader.frag";
    if(fragLoc.empty()){
        std::cerr << "cannot find fragment shader" << std::endl;
        return -1;
    }
    
    //load shader files into string
    auto vertexShaderCode = loadFile(vertLoc);
    if(vertexShaderCode == ""){
        std::cerr << "error while reading vertex shader file" << std::endl;
        return -1;
    }
    //std::cout << vertexShaderCode;
    
    auto fragmentShaderCode = loadFile(fragLoc);
    if(fragmentShaderCode == ""){
        std::cerr << "error while reading fragment shader file" << std::endl;
        return -1;
    }
    //std::cout << fragmentShaderCode;
    
    //compile the shaders
    unsigned int vertexShader = 0;
    if( compileShader(vertexShaderCode, vertexShader, GL_VERTEX_SHADER) != 0){
        std::cerr << "error while compiling vertex shader" << std::endl;
        return -1;
    }
    
    unsigned int fragmentShader = 0;
    if( compileShader(fragmentShaderCode, fragmentShader, GL_FRAGMENT_SHADER) != 0){
        std::cerr << "error while compiling fragment shader" << std::endl;
        return -1;
    }
    
    //link the shaders to a shader program
    int success;
    
    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n"
                  << infoLog << std::endl;
        return -1;
    }
    
    
    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    
    return 0;
}

std::string sakurajin::loadFile ( std::filesystem::path location ) {
    std::string shaderCode;
    std::ifstream ShaderFile;
    // ensure ifstream objects can throw exceptions:
    ShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        // open files
        ShaderFile.open(location);
        std::stringstream ShaderStream;
        // read file's buffer contents into streams
        ShaderStream << ShaderFile.rdbuf();
        // close file handlers
        ShaderFile.close();
        // convert stream into string
        shaderCode = ShaderStream.str();
    } catch (const std::ifstream::failure &e) {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
        return "";
    }
    
    return shaderCode;
}

int sakurajin::compileShader( std::string code, unsigned int& shaderLoc, int shaderType ) {
    // compile the shader and return the location of the result
    int success;

    auto shaderCode = code.c_str();

    auto shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &shaderCode, NULL);
    glCompileShader(shader);

    // print compile errors if anything went wrong
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::COMPILATION_FAILED\n"
                  << infoLog << std::endl;
        std::cerr << "Shader source code:" << shaderCode << std::endl;
        return -1;
    };

    shaderLoc = shader;
    return 0;
}

