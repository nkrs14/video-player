# Video Player demo

This is a demo on how to write a basic video player with ffmpeg and glfw3.

All credits for the example video go to Alexander Lehmann who published the video under the CC By 4.0 license.
You can find the original video files [here](https://media.ccc.de/v/rc3-extras-1980-reclaim-your-face).

