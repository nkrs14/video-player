#pragma once

//video dependencies
#include <glad/glad.h> 
#include "GLFW/glfw3.h"

#include <string>
#include <filesystem>

namespace sakurajin{
    void framebuffer_size_callback(GLFWwindow* window, int width, int height);

    void processInput(GLFWwindow *window);

    GLFWwindow* create_window(bool& error);

    void cleanup();
    
    int createShader(unsigned int& shaderProgram);
    
    int compileShader(std::string code, unsigned int& shaderLoc, int shaderType);
    
    std::string loadFile(std::filesystem::path location);
}
