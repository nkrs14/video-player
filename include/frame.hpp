#pragma once

extern "C"{
    #include <libavcodec/avcodec.h>
    #include <libavutil/imgutils.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
}
        
namespace sakurajin{
    class Frame{
    private:
        AVFrame* frameData = nullptr;
    public:
        /**
        * @brief Construct a new frame from given data
        * 
        * @param dat the data this frame will contain
        */
        Frame(AVFrame* dat);
        
        /**
        * @brief retrieve a pointer to the internal data for the FFmpeg backend
        * 
        */
        AVFrame* data() const;
        
        /**
        * Convert a frame to an AVFrame without needing to call dataFF
        * 
        */
        operator AVFrame*();
        
        /**
        * directly assign an AVFrame to the Frame to set the value
        */
        Frame& operator=(AVFrame* x);
        
         /**
        * @brief Construct an empty frame.
        * 
        */
        Frame();
        
        /**
        * free all of the data in this Frame object.
        */
        ~Frame();

        /**
        * @brief Checks if the frame data is empty
        * 
        * @return true the frame data is empty
        * @return false the frame data has contents
        */
        bool isEmpty() const;
        
        /**
        * free all of the data in this Frame object.
        */
        void clear();
        
    };
}
